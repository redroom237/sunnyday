/**
 * 
 */
package com.gg.social.presentation;

import java.util.Scanner;

/**
 * @author sgabello
 *
 * Represent the view of the application.
 * Reads user inputs and write back a text output 
 */
public class Console {

	private final Scanner scanner;

  public Console(Scanner scanner) {
    this.scanner = scanner;
  }

  public void printMessage(final String text) {
    System.out.println(text);
  }

  public void prompt() {
    System.out.print("> ");
  }

  public String nextLine() {
    return scanner.nextLine();
  }
}
