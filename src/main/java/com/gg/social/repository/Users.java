/**
 * 
 */
package com.gg.social.repository;

import java.util.HashSet;
import java.util.Set;

import com.gg.social.domain.User;

/**
 * @author sgabello
 *
 * An in memory user repository implementation.
 * 
 */
public class Users implements IUserRepository {
	
	private Set<User> userSet = new HashSet<>();

  @Override
  public void save(User user) {
    userSet.add(user);
  }

  @Override
  public User findUserByUsername(String username) {
  	return userSet.stream().filter(user -> user.getUsername().equalsIgnoreCase(username)).findFirst().orElse(null);
  }

}
