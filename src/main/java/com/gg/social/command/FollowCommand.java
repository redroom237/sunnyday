/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;
import com.gg.social.domain.UserService;

/**
 * @author sgabello
 * 
 * Implements the Command interface and execute a follows command.
 * 
 */
public class FollowCommand implements Command{

	private static final String FOLLOWS = " follows "; 
	
	UserService userService;
	
	public FollowCommand(UserService userService) {
		this.userService = userService;
	}
	@Override
	public List<Post> execute(String commandLine){
		
		String[] inputParameters = commandLine.split(FOLLOWS);

		String username = inputParameters[0].trim();
		String followed = inputParameters[1].trim();
		
		userService.follows(username, followed);
		
		return emptyList;
	}	

}
