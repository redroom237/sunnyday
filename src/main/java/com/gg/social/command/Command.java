/**
 * 
 */
package com.gg.social.command;

import java.util.ArrayList;
import java.util.List;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 * Executes the input user command and provide a list of Post object
 * as result.
 * The list of post might be empty 
 * 
 */
public interface Command {
	
	static final List<Post> emptyList = new ArrayList<>();
	
	List<Post> execute(String commandLine);

}
