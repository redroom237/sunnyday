/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 * Parse an input and retrieve a command object by using 
 * a CommandFactory object.
 * The retrieved command is then execute. 
 * 
 */
public class CommandParser {
	
	private CommandFactory commandFactory;
	/**
	 * 
	 */
	public CommandParser(CommandFactory commandFactory) {
		this.commandFactory = commandFactory;
	}
	
	public List<Post> execute(String commandLine){
		Command command = commandFactory.createCommand(commandLine);
		List<Post> posts = command.execute(commandLine);
		
		return posts;
	}

}
