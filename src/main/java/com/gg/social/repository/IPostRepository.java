/**
 * 
 */
package com.gg.social.repository;

import java.util.List;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 * 
 * Defines post persistence operation.
 *
 */
public interface IPostRepository {
	
	void save(Post post);
	
	List<Post> findPostsByUsername(String username);

}
