package com.gg.social.command;
import static org.junit.Assert.*;

import java.time.Clock;

import org.junit.Before;
import org.junit.Test;

import com.gg.social.domain.UserService;
import com.gg.social.repository.IPostRepository;
import com.gg.social.repository.IUserRepository;
import com.gg.social.repository.Posts;
import com.gg.social.repository.Users;

/**
 * 
 */

/**
 * @author sgabello
 *
 */
public class CommandFactoryTest {
	
	private static final String USER = "Alice";
	private static final String MESSAGE = "I love the weather today";
	private static final String POST_COMMAND = USER + " -> " + MESSAGE;
	private static final String READ_COMMAND = USER;
	private static final String FOLLOWS_COMMAND = "Charlie follows Alice";
	private static final String WALL_COMMAND = "Charlie wall";
	private static final String EXIT_COMMAND = "exit";

	private UserService userService;
	private IUserRepository users;
	private IPostRepository posts;
	private CommandFactory commandFactory;
	private CommandParser commandParser;
	private String commandLine;
	private Clock clock;
	
	@Before
	public void setUp(){
		clock = Clock.systemUTC();
		users = new Users();
		posts = new Posts();
		userService = new UserService(users, posts, clock);
		commandFactory = new CommandFactory(userService);
		commandParser = new CommandParser(commandFactory);
	}
	
	@Test
	public void commandParser_execute_returnExitCommand(){
		commandLine = EXIT_COMMAND;
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof ExitCommand);
	}
	@Test
	public void commandParser_execute_returnPostCommand(){
		commandLine = POST_COMMAND;
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof PostCommand);
	}
	@Test
	public void commandParser_execute_returnReadCommand(){
		commandLine = READ_COMMAND;
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof ReadCommand);
	}
	@Test
	public void commandParser_execute_returnFollowsCommand(){
		commandLine = FOLLOWS_COMMAND;
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof FollowCommand);
	}
	@Test
	public void commandParser_execute_returnWallCommand(){
		commandLine = WALL_COMMAND;
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof WallCommand);
	}
	@Test
	public void commandParser_execute_returnInvalidCommand(){
		commandLine = "Al1c3";
		Command command = commandFactory.createCommand(commandLine);
		assertTrue(command instanceof InvalidCommand);
	}
}
