/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 * 
 * Implements the Command interface and executes the end application command.
 *
 */
public class ExitCommand implements Command {

	@Override
	public List<Post> execute(String commandLine){
		System.exit(0);
		return null;
	}

}
