/**
 * 
 */
package com.gg.social.presentation;

import java.time.Clock;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 * Formats read post messages.
 */
public class ReadPostFormatter extends PostFormatter {

	/**
	 * @param clock
	 */
	public ReadPostFormatter(Clock clock) {
		super(clock);
	}

	@Override
	public String format(Post post){
		return super.format(post);
	}

}
