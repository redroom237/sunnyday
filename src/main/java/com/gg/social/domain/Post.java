/**
 * 
 */
package com.gg.social.domain;

import java.time.Instant;

/**
 * @author sgabello
 *
 * A post containing user messages
 * 
 */
public class Post implements Comparable<Post>{

	private String userName;
	private String message;
	private Instant created;

	/*
	 * @param user the user who publishes the post
	 * @param message the text message typed by the user
	 * @param created the publication instant
	 */
	public Post(String user, String message, Instant created) {
		this.userName = user;
		this.message = message;
		this.created = created;
	}
	
	public String getMessage(){
		return message;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getUserName(){
		return userName;
	}

	public Instant getCreated(){
		return created;
	}
	
	@Override
  public int compareTo(Post onotherPost) {
    return onotherPost.getCreated().compareTo(created);
  }

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
}
