/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 * 
 * Implements the Command interface and execute an invalid command.
 *
 */
public class InvalidCommand implements Command {

	@Override
	public List<Post> execute(String commandLine){
		return emptyList;
	}

}
