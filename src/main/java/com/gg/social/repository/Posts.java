/**
 * 
 */
package com.gg.social.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gg.social.domain.Post;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * @author sgabello
 * 
 * An in memory post repository implementation.
 *
 */
public class Posts implements IPostRepository {
	
	private Multimap<String, Post> postMap = ArrayListMultimap.create();

	@Override
	public void save(Post post){
		postMap.put(post.getUserName(), post);
	}

	@Override
	public List<Post> findPostsByUsername(String username){
		// TODO Auto-generated method stub
		return Collections.unmodifiableList(new ArrayList<>(postMap.get(username)));
	}

}
