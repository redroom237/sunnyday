package com.gg.social.init;

import java.time.Clock;
import java.util.Scanner;

import com.gg.social.command.CommandFactory;
import com.gg.social.command.CommandParser;
import com.gg.social.domain.UserService;
import com.gg.social.presentation.Console;
import com.gg.social.presentation.PostFormatterSelector;
import com.gg.social.repository.IPostRepository;
import com.gg.social.repository.IUserRepository;
import com.gg.social.repository.Posts;
import com.gg.social.repository.Users;

/**
 * Initializes the application and starts it by calling App.run() method
 *
 */
public class AppInizializer {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args){

		Clock clock = Clock.systemUTC();
		Console console = new Console(scanner);
		IUserRepository users = new Users();
		IPostRepository posts = new Posts();
		UserService userService = new UserService(users, posts, clock);
		CommandFactory commandFactory = new CommandFactory(userService);
		CommandParser commandParser = new CommandParser(commandFactory);
		PostFormatterSelector formatterSelector = new PostFormatterSelector(clock);

		new App(console, commandParser, formatterSelector).run();
	}
}
