/**
 * 
 */
package com.gg.social.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.gg.social.domain.User;
import com.google.common.collect.Sets;

/**
 * @author sgabello
 *
 */
public class UserTest {
	
	private static final String ALICE_USER = "Alice";
	private static final String BOB_USER = "Bob";
	private static final String CHARLIE_USER = "Charlie";
	
	private User user;
	@Before
	public void setUp(){
		user = new User(CHARLIE_USER);
	}
	
	@Test
	public void username(){
		assertEquals(user.getUsername(), CHARLIE_USER);
	}
	
	@Test
	public void oneFollowed(){
		user.follows(ALICE_USER);
		assertEquals(Sets.newHashSet(ALICE_USER), user.getFollowedList());
	}
	@Test
	public void twoFollowed(){
		user.follows(ALICE_USER);
		user.follows(BOB_USER);
		assertEquals(Sets.newHashSet(ALICE_USER, BOB_USER), user.getFollowedList());
	}
	@Test
	public void sameFollower(){
		user.follows(BOB_USER);
		user.follows(BOB_USER);
		assertEquals( Sets.newHashSet(BOB_USER), user.getFollowedList());
	}
	@Test
	public void follows_himself_returnFollowedEmptyList(){
		user.follows(CHARLIE_USER);
		assertTrue(user.getFollowedList().isEmpty());
	}

}
