/**
 * 
 */
package com.gg.social.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sgabello
 * 
 * A user that interacts with the application.
 * Provides a list of followed user names that refers
 * to other user objects.
 * 
 */
public class User {

	private String username;
	private Set<String> followed = new HashSet<>();
	
	public User(String username) {
		this.username = username;
	}

	public String getUsername(){
		return username;
	}
	
	public void follows(String username){
		if (this.username.equalsIgnoreCase(username))
			return;
		followed.add(username);
	}

	public Set<String> getFollowedList(){
		return followed;
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	

}
