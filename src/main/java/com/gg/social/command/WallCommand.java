/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;
import com.gg.social.domain.UserService;

/**
 * @author sgabello
 *
 * Implements the Command interface and execute an wall command.
 * 
 */
public class WallCommand implements Command{
	
	private static final String WALL = " wall"; 
	
	UserService userService;

	public WallCommand(UserService userService) {
		this.userService = userService;
	}

	@Override
	public List<Post> execute(String commandLine){
		
		String[] inputParameters = commandLine.split(WALL);
		
		String username = inputParameters[0].trim();
		
		return userService.wall(username);
	}

}
