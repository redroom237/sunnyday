/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;
import com.gg.social.domain.UserService;

/**
 * @author sgabello
 * 
 * Implements the Command interface and execute an read command.
 * 
 */
public class ReadCommand implements Command{
	
	UserService userService;
	
	public ReadCommand(UserService userService) {
		this.userService = userService;		
	}
	
	public List<Post> execute(String username){
		return  userService.getPosts(username);
	}

}
