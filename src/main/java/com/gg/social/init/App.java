/**
 * 
 */
package com.gg.social.init;

import java.util.List;

import com.gg.social.command.CommandParser;
import com.gg.social.domain.Post;
import com.gg.social.presentation.Console;
import com.gg.social.presentation.PostFormatter;
import com.gg.social.presentation.PostFormatterSelector;

/**
 * @author sgabello
 * 
 * Keeps the application running with a loop that reads
 * user commands from the console and pass them to the
 * command parser.
 * Once the command is executed the output is formatted and sent to the console.
 * The loop ends when the user enter 'exit'.  
 *
 */
public class App {
	
	private Console console;
	private CommandParser commandParser;
	private PostFormatterSelector formatterSelector;
	
	
	public App(Console console, CommandParser commandParser, PostFormatterSelector formatterSelector) {
		this.console = console;
		this.commandParser = commandParser;
		this.formatterSelector = formatterSelector;
	}
	
	public void run(){
		while (true) {
			console.prompt();
			String commandLine = console.nextLine();
			List<Post> postList = commandParser.execute(commandLine);
			PostFormatter formatter = formatterSelector.getFormatter(commandLine);
			postList.stream()
			.forEach(p -> console.printMessage(formatter.format(p)));
		}
	}

}
