/**
 * 
 */
package com.gg.social.command;

import java.util.regex.Pattern;

import com.gg.social.domain.UserService;

/**
 * @author sgabello
 *
 * Retrieve a Command object by parsing a user command that matches with 
 * a predetermined pattern.
 * 
 */
public class CommandFactory {
	
	private static final Pattern POST_COMMAND_PATTERN = Pattern.compile("^(\\S+) -> (.+)$");
	private static final Pattern READ_COMMAND_PATTERN = Pattern.compile("^[a-zA-Z]+$");
	private static final Pattern FOLLOW_COMMAND_PATTERN = Pattern.compile("^(\\S+) follows (\\S+)$");
	private static final Pattern WALL_COMMAND_PATTERN = Pattern.compile("^(\\S+) wall$");
	private static final Pattern EXIT_COMMAND_PATTERN = Pattern.compile("^exit$");
	
	private UserService users;
	
	/**
	 * 
	 */
	public CommandFactory(UserService users) {
		this.users = users;
	}
	
	public Command createCommand(String commandLine){
		if (matches(EXIT_COMMAND_PATTERN, commandLine))
			return new ExitCommand();
		if (matches(POST_COMMAND_PATTERN, commandLine))
			return new PostCommand(users);
		if (matches(READ_COMMAND_PATTERN, commandLine))
			return new ReadCommand(users);
		if (matches(FOLLOW_COMMAND_PATTERN, commandLine))
			return new FollowCommand(users);
		if (matches(WALL_COMMAND_PATTERN, commandLine))
			return new WallCommand(users);
		return new InvalidCommand();
	}
	
	private boolean matches(Pattern pattern, String commandLine){
		return pattern.matcher(commandLine).find();
	}
}
