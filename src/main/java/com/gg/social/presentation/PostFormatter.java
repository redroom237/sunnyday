/**
 * 
 */
package com.gg.social.presentation;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 * Format a post message adding a time elapsed calculation.
 */
public abstract class PostFormatter {
	
	private Clock clock;
	
	public PostFormatter(Clock clock) {
		this.clock = clock;
	}
	
	public String format(Post post){
		return post.getMessage() + " " + formatTimeElapsed(post.getCreated());
	}
	
	private String formatTimeElapsed(Instant created){
		Duration timeElapsed = Duration.between(created, clock.instant());
		
		//within a minute
		if (timeElapsed.toMinutes() == 0)
			return unitFormat(timeElapsed.getSeconds(), "second");
		//within a hour
		if (timeElapsed.toHours() == 0)
			return unitFormat(timeElapsed.toMinutes(), "minute");
		//within a day
		if (timeElapsed.toDays() == 0)
			return unitFormat(timeElapsed.toHours(), "hour");
		//more than a day
		return unitFormat(timeElapsed.toDays(), " day");
		
	}
	
	private String unitFormat(long unitDurarion, String unit){
		return "(" + unitDurarion  + " " + (unitDurarion > 1 ? unit + "s" : unit) + " ago)";
		
	}
	
}
