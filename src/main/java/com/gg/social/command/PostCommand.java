/**
 * 
 */
package com.gg.social.command;

import java.util.List;

import com.gg.social.domain.Post;
import com.gg.social.domain.UserService;

/**
 * @author sgabello
 * 
 * Implements the Command interface and execute a post command.
 * 
 */
public class PostCommand implements Command{

	private static final String POST = " -> ";

	UserService userService;
	
	public PostCommand(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public List<Post> execute(String commandLine){
		
		String[] inputParameters = commandLine.split(POST);
		
		String username = inputParameters[0].trim();
		String message = inputParameters[1].trim();
		
		userService.savePost(username, message);
		
		return emptyList;
	}
	
}
