/**
 * 
 */
package com.gg.social.presentation;

import java.time.Clock;
import java.util.regex.Pattern;

/**
 * @author sgabello
 *
 * Creates a read/wall post formatter depending on the input user command.
 */
public class PostFormatterSelector {

	private static final Pattern WALL_COMMAND_PATTERN = Pattern.compile("^(\\S+) wall$");
	
	private WallPostFormatter wallFormatter; 
	private ReadPostFormatter readPostFormatter;
	
	private Clock clock;
	/**
	 * 
	 */
	public PostFormatterSelector(Clock clock) {
		this.clock = clock;
		wallFormatter = new WallPostFormatter(clock);
		readPostFormatter = new ReadPostFormatter(clock);
	}
	public PostFormatter getFormatter(String commandLine){
		if (matches(WALL_COMMAND_PATTERN, commandLine))
			return wallFormatter;
		else return readPostFormatter;
	}
	
	private boolean matches(Pattern pattern, String commandLine){
		return pattern.matcher(commandLine).find();
	}

}
