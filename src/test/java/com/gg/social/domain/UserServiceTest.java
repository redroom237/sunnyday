/**
 * 
 */
package com.gg.social.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.gg.social.repository.IPostRepository;
import com.gg.social.repository.IUserRepository;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;


/**
 * @author sgabello
 *
 */
public class UserServiceTest {
	
	private static final String ALICE_USERNAME = "Alice";
	private static final String CHARLIE_USERNAME = "Charlie";
	private static final String BOB_USERNAME = "Bob";
	private static final String MESSAGE = "I love the wether today";
	private static final String I_LOVE_THE_WETHER_TODAY = "I love the weather today";
	private static final String COFFE_IN_NEW_YORK = "I'm in New York today! Anyone want to have a coffee?";
	private static final String DAMMN_WE_HAVE_LOST = "Damn! We lost!";
	private static final String GOOD_GAME = "Good game though.";
	private static final Instant NOW = Instant.now();
	private static final java.time.Instant FIFTEEN_SECONDS_AGO = NOW.minus(15, ChronoUnit.SECONDS);
	private static final java.time.Instant ONE_MINUTE_AGO = NOW.minus(1, ChronoUnit.MINUTES);
	private static final java.time.Instant TWO_MINUTES_AGO = NOW.minus(2, ChronoUnit.MINUTES);
	private static final java.time.Instant FIVE_MINUTES_AGO = NOW.minus(5, ChronoUnit.MINUTES);

	private static List<Post> charliePosts = Arrays.asList(new Post(CHARLIE_USERNAME, COFFE_IN_NEW_YORK, FIFTEEN_SECONDS_AGO));
	private static List<Post> bobPosts = Arrays.asList(new Post(BOB_USERNAME, DAMMN_WE_HAVE_LOST, TWO_MINUTES_AGO), new Post(BOB_USERNAME, GOOD_GAME, ONE_MINUTE_AGO));
	private static List<Post> alicePosts = Arrays.asList(new Post(ALICE_USERNAME, I_LOVE_THE_WETHER_TODAY, FIVE_MINUTES_AGO));
  
	private User user;
	private Post post;
	@Mock IUserRepository mockUsers;
	@Mock IPostRepository mockPosts;
	@Mock Clock clock;

	private UserService userService;
	@Before
	public void setUp(){
		clock = Clock.systemUTC();
	  user = new User(ALICE_USERNAME);
		post = new Post(ALICE_USERNAME, MESSAGE, clock.instant());
		
		initMocks(this);
		when(clock.instant()).thenReturn(NOW);
	  userService = new UserService(mockUsers, mockPosts, clock);
	}
	
	@Test
	public void saveUser(){
		userService.saveUser(ALICE_USERNAME);
		verify(mockUsers).save(user);
	}
	
	@Test
	public void findUser(){
		userService.getUser(ALICE_USERNAME);
		verify(mockUsers).findUserByUsername(ALICE_USERNAME);
	}
	
	@Test
	public void savePost(){
	  when(mockUsers.findUserByUsername(ALICE_USERNAME)).thenReturn(user);
	  userService.savePost(ALICE_USERNAME, MESSAGE);
	  Post post = new Post(ALICE_USERNAME, MESSAGE, clock.instant());
		verify(mockPosts).save(post);
	}
	
	@Test
	public void getPost(){
		userService.getPosts(ALICE_USERNAME);
		verify(mockPosts).findPostsByUsername(ALICE_USERNAME);
	}
	
	@Test
	public void getPosts_twoPosts_returnOrderedPostList(){
		
		when(mockPosts.findPostsByUsername(BOB_USERNAME)).thenReturn(bobPosts);
		List<Post> bobTimeLine = userService.getPosts(BOB_USERNAME);
		
		Post bobPost1 = new Post(BOB_USERNAME, DAMMN_WE_HAVE_LOST, TWO_MINUTES_AGO);
		Post bobPost2 = new Post(BOB_USERNAME, GOOD_GAME, ONE_MINUTE_AGO);
		List<Post> orderedPost = Arrays.asList(bobPost2, bobPost1);
		
		Assert.assertEquals(orderedPost, bobTimeLine);
	}
	
	@Test
	public void wall_userAndFollowersPosts_returnsOrderedPostList(){
		User charlie = new User(CHARLIE_USERNAME);
    charlie.follows(ALICE_USERNAME);
    charlie.follows(BOB_USERNAME);
    when(mockUsers.findUserByUsername(CHARLIE_USERNAME)).thenReturn(charlie);
    
		when(mockPosts.findPostsByUsername(ALICE_USERNAME)).thenReturn(alicePosts);
		when(mockPosts.findPostsByUsername(BOB_USERNAME)).thenReturn(bobPosts);
		when(mockPosts.findPostsByUsername(CHARLIE_USERNAME)).thenReturn(charliePosts);
		
		List<Post> wallPosts = userService.wall(CHARLIE_USERNAME);
	
		Post alicePost = new Post(ALICE_USERNAME, I_LOVE_THE_WETHER_TODAY, FIVE_MINUTES_AGO);
		Post bobPost1 = new Post(BOB_USERNAME, DAMMN_WE_HAVE_LOST, TWO_MINUTES_AGO);
		Post bobPost2 = new Post(BOB_USERNAME, GOOD_GAME, ONE_MINUTE_AGO);
		Post charliePost = new Post(CHARLIE_USERNAME, COFFE_IN_NEW_YORK, FIFTEEN_SECONDS_AGO);
		
		List<Post> orderedWallList = Arrays.asList(charliePost, bobPost2, bobPost1, alicePost);
		
		Assert.assertEquals(orderedWallList, wallPosts);
		
		verify(mockUsers).findUserByUsername(CHARLIE_USERNAME);
		
		verify(mockPosts).findPostsByUsername(CHARLIE_USERNAME);
		verify(mockPosts).findPostsByUsername(BOB_USERNAME);
		verify(mockPosts).findPostsByUsername(ALICE_USERNAME);	
	}
}
