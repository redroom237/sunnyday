/**
 * 
 */
package com.gg.social.domain;


import java.time.Clock;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gg.social.repository.IPostRepository;
import com.gg.social.repository.IUserRepository;

/**
 * @author sgabello
 *
 * A user service using both user/post repository.
 * 
 */
public class UserService {
	
	private IUserRepository users;
	private IPostRepository posts;
	private Clock clock;
	
	public UserService(IUserRepository users, IPostRepository posts, Clock clock) {
		this.users = users;
		this.posts = posts;
		this.clock = clock;
	}
	//USERS
	public void saveUser(String username){
		users.save(new User(username));
	}
	
	public User getUser(String username){
		return users.findUserByUsername(username);
	}
	
	// POSTS
	public void savePost(String username, String message){
		users.save(new User(username));
		User user = users.findUserByUsername(username);
		Post post = new Post(user.getUsername(), message, clock.instant());
		posts.save(post);
	}
	
	//READ
	public List<Post> getPosts(String username){
		return posts.findPostsByUsername(username).stream().sorted().collect(Collectors.toList());
	}
	
	//FOLLOWERS
	public void follows(String username, String followedUserName){
		User user = users.findUserByUsername(username);
		User followed = users.findUserByUsername(followedUserName);
		
		user.follows(followed.getUsername());
	}
	
	//WALL
	public List<Post> wall(String username){
		
		User user = users.findUserByUsername(username);
		List<Post> userPosts = posts.findPostsByUsername(user.getUsername());
		
		List<Post> followedPosts = user.getFollowedList().stream().map(f -> posts.findPostsByUsername(f))
				.flatMap(postList -> postList.stream()).collect(Collectors.toList());

		List<Post> wallList = Stream.concat(userPosts.stream(), followedPosts.stream())
				.sorted().collect(Collectors.toList());

		return wallList;
	}

	
}
