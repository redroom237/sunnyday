/**
 * 
 */
package com.gg.social.repository;

import static org.junit.Assert.*;

import java.time.Clock;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.gg.social.domain.Post;
import com.gg.social.domain.User;

/**
 * @author sgabello
 *
 */
public class PostsTest {

	private static final String ALICE_USERNAME = "Alice";
	private static final String MESSAGE = "I love the wether today";
	private Post post;
	private IPostRepository posts;
	private static final Instant NOW = Instant.now();
	
	@Before
	public void setUp(){
		post = new Post(ALICE_USERNAME, MESSAGE, NOW);
		posts = new Posts();
	}
	@Test
	public void postOnMessage(){
		posts.save(post);
		List<Post> postList = Arrays.asList(post);
		
		List<Post> userPosts = posts.findPostsByUsername(ALICE_USERNAME);
		assertEquals(postList, userPosts);
	}
	@Test
	public void postTwoMessages(){
		Post post1 = new Post(ALICE_USERNAME, MESSAGE + 1, NOW);
		Post post2 = new Post(ALICE_USERNAME, MESSAGE + 2, NOW);
		List<Post> postList = Arrays.asList(post1, post2);
		posts.save(post1);
		posts.save(post2);
		List<Post> userPosts = posts.findPostsByUsername(ALICE_USERNAME);
		assertEquals(postList, userPosts);
	}
}
