/**
 * 
 */
package com.gg.social.presentation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 */
public class PostFormatterTest {
	
	private static final String READ_COMMAND = "Alice";
	private static final String WALL_COMMAND = "Charlie wall";
	//
	private static final String ALICE_USERNAME = "Alice";
	private static final String CHARLIE_USERNAME = "Charlie";
	private static final String BOB_USERNAME = "Bob";
	private static final String I_LOVE_THE_WETHER_TODAY = "I love the weather today";
	private static final String COFFE_IN_NEW_YORK = "I'm in New York today! Anyone wants to have a coffee?";
	private static final String DAMMN_WE_HAVE_LOST = "Damn! We lost!";
	private static final String GOOD_GAME = "Good game though.";
	//
	private static final Instant NOW = Instant.now();
	private static final java.time.Instant FIFTEEN_SECONDS_AGO = NOW.minus(15, ChronoUnit.SECONDS);
	private static final java.time.Instant ONE_MINUTE_AGO = NOW.minus(1, ChronoUnit.MINUTES);
	private static final java.time.Instant TWO_MINUTES_AGO = NOW.minus(2, ChronoUnit.MINUTES);
	private static final java.time.Instant FIVE_MINUTES_AGO = NOW.minus(5, ChronoUnit.MINUTES);

	private String commandLine;
	private PostFormatterSelector formatterSelector;
	@Mock Console console;
	@Mock Clock clock;
	
	@Before
	public void setUp(){
		initMocks(this);
		when(clock.instant()).thenReturn(NOW);
		formatterSelector = new PostFormatterSelector(clock);
	}

	@Test
	public void formatReadPostList(){
		commandLine = READ_COMMAND;
		
		PostFormatter formatter = formatterSelector.getFormatter(commandLine);
		
		assertTrue(formatter instanceof ReadPostFormatter);
		
		Post alicePost = new Post(ALICE_USERNAME, I_LOVE_THE_WETHER_TODAY, FIVE_MINUTES_AGO);
		Post bobPost1 = new Post(BOB_USERNAME, DAMMN_WE_HAVE_LOST, TWO_MINUTES_AGO);
		Post bobPost2 = new Post(BOB_USERNAME, GOOD_GAME, ONE_MINUTE_AGO);
		
		List<Post> aliceReadPost = Arrays.asList(alicePost);
		
		aliceReadPost.stream()
		.forEach(p -> console.printMessage(formatter.format(p)));
				
		List<Post> bobReadPost = Arrays.asList(bobPost2, bobPost1);
		
		bobReadPost.stream()
		.forEach(p -> console.printMessage(formatter.format(p)));
		
		InOrder inOrder = inOrder(console);
  	inOrder.verify(console).printMessage("I love the weather today (5 minutes ago)");
		inOrder.verify(console).printMessage("Good game though. (1 minute ago)");
    inOrder.verify(console).printMessage("Damn! We lost! (2 minutes ago)");
	}

	@Test
	public void formatWallPostList(){
		commandLine = WALL_COMMAND;
		
		PostFormatter formatter = formatterSelector.getFormatter(commandLine);
		
		assertTrue(formatter instanceof WallPostFormatter);
		
		Post alicePost = new Post(ALICE_USERNAME, I_LOVE_THE_WETHER_TODAY, FIVE_MINUTES_AGO);
		Post bobPost1 = new Post(BOB_USERNAME, DAMMN_WE_HAVE_LOST, TWO_MINUTES_AGO);
		Post bobPost2 = new Post(BOB_USERNAME, GOOD_GAME, ONE_MINUTE_AGO);
		Post charliePost = new Post(CHARLIE_USERNAME, COFFE_IN_NEW_YORK, FIFTEEN_SECONDS_AGO);
				
		List<Post> wallList = Arrays.asList(charliePost, bobPost2, bobPost1, alicePost);

		wallList.stream()
		.forEach(p -> console.printMessage(formatter.format(p)));
		
		InOrder inOrder = inOrder(console);
		inOrder.verify(console).printMessage("Charlie - I'm in New York today! Anyone wants to have a coffee? (15 seconds ago)");
    inOrder.verify(console).printMessage("Bob - Good game though. (1 minute ago)");
    inOrder.verify(console).printMessage("Bob - Damn! We lost! (2 minutes ago)");
    inOrder.verify(console).printMessage("Alice - I love the weather today (5 minutes ago)");
		
	}
}
