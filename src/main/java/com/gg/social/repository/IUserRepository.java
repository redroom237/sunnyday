/**
 * 
 */
package com.gg.social.repository;

import com.gg.social.domain.User;

/**
 * @author sgabello
 * 
 * Defines user persistence operation.
 *
 */
public interface IUserRepository {
	
	 public void save(User user);

	 public User findUserByUsername(String username);

}
