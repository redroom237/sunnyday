/**
 * 
 */
package com.gg.social.presentation;

import java.time.Clock;

import com.gg.social.domain.Post;

/**
 * @author sgabello
 *
 * Formats wall post messages.
 */
public class WallPostFormatter extends PostFormatter {

	
	public WallPostFormatter(Clock clock) {
		super(clock);
	}
	
	@Override
	public String format(Post post){
		return post.getUserName() + " - " + super.format(post);
	}

}
