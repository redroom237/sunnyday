/**
 * 
 */
package com.gg.social.repository;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.gg.social.domain.User;
import com.gg.social.repository.Users;

/**
 * @author sgabello
 *
 */
public class UsersTest {
	
	private static final String ALICE_USERNAME = "Alice";
	private User user;
	private Users users;
	
	@Before
	public void setUp(){
	  user = new User(ALICE_USERNAME);
		users = new Users();
	}
	
	@Test
	public void saveUser(){
		users.save(user);
		User foundUser = users.findUserByUsername(user.getUsername());
		assertNotNull(foundUser);	
		assertEquals(user.getUsername(), foundUser.getUsername());
	}
	@Test
	public void getUser_unexistingUser_returns_null(){
		User foundUser = users.findUserByUsername(user.getUsername());
		assertNull(foundUser);
	}
}
